﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Myaso.Models;
using Myaso.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Myaso.Controllers
{
    public class HomeController : Controller
    {
        private ApplictionDbContext Context;

        public HomeController(ApplictionDbContext applictionDbContext)
        {
            Context = applictionDbContext;
        }

        public IActionResult Index(int weekId = 1, string planName = "Fit", string dayName = "Monday", string faqName = "Продукты")
        {

            PlanViewModel model = GetModel(weekId, planName, dayName, faqName);
            
            return View(model);
        }

        public IActionResult GetDishes(int weekId = 1, string planName = "Fit",
            string dayName = "Monday")
        {
            PlanViewModel model = GetModel(weekId, planName, dayName);
            return PartialView("_GetDishes", model);
        }

        public string Test(int weekId = 1, string planName = "Fit", string dayName = "Monday", string faqName = "Продукты")
        {
            PlanViewModel model = GetModel(weekId, planName, dayName, faqName);
            return JsonConvert.SerializeObject(model,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        public PlanViewModel GetModel(int weekId = 1, string planName = "Fit", string dayName = "Monday", string faqName = "Продукты")
        {
            Plan plan = Context.Plans.First(p => p.Name == planName);
            Day day = Context.Days.First(p => p.NameEn == dayName);
            Week week = Context.Weeks.First(w => w.Id == weekId);
            Faq faq = Context.Faqs.FirstOrDefault(f => f.Name == faqName);
            PlanToDay planToDay = Context.PlanToDays
                .Include(p => p.Plan)
                .Include(p => p.Day)
                .Include(p => p.Week)
                .FirstOrDefault(p => p.PlanId == plan.Id && p.DayId == day.Id && p.WeekId == week.Id);
            List<DishToPlan> dishToPlan = Context.DishToPlans
                .Include(d => d.Dish)
                .Include(d => d.Food)
                .Where(d => d.PlanToDayId == planToDay.Id).ToList();
            int i = 0;
            DateTime current = DateTime.Today;
            DateTime firstDayOfWeek = new DateTime();
            switch (current.DayOfWeek.ToString())
            {
                case "Понедельник":
                    firstDayOfWeek = current;
                    break;
                case "Sunday":
                    firstDayOfWeek = current.AddDays(-6);
                    break;

            }
            //DateTime firstDayOfMonth = new DateTime(current.Year, current.Month, 1);
            //var diff = (current - firstDayOfMonth).TotalDays;
            //double days = Convert.ToDouble(diff);
            //int currentWeek = Convert.ToInt32(Math.Ceiling(days / 7));
            //if (week > 0 & week < 5)
            //{

            //}
            //int nextweek = week > 0 ? 1 : week / 4;
            //DateTime firstDayOfWeek = i;
            //List<DateTime> test = new List<DateTime>();
            //DateTime a = new DateTime(current.Year, current.Month, i);
            //test.Add(firstDayOfWeek);
            //for (int j = 1; j < 7; j++)
            //{
            //    DateTime nextDay = firstDayOfWeek.AddDays(1);
            //    test.Add(nextDay);
            //}
            PlanViewModel model = new PlanViewModel()
            {
                User = Context.Users.FirstOrDefault(u => u.Phone == HttpContext.User.Identity.Name),
                Plan = plan,
                OrderDesc = Context.OrderDescs.FirstOrDefault(o => o.PlanId == plan.Id),//надо сделать при выборе 5-7 принималась цена
                Day = day,
                Plans = Context.Plans.ToList(),
                Days = Context.Days.ToList(),
                PlanToDay = planToDay,
                DishToPlans = dishToPlan,
                Weeks = Context.Weeks.ToList(),
                Week = week,
                FirstDay = firstDayOfWeek,
                Faqs = Context.Faqs.Include(f => f.FaqDescriptions).ToList(),
                FaqDescriptions = Context.FaqDescriptions.Where(f => f.FaqId == faq.Id).ToList(),
                Faq = faq
            };
            return model;
        }

        public string SelectDays(string planName, string daysQty)
        {
            Plan plan = Context.Plans.FirstOrDefault(p => p.Name == planName);
            OrderDesc order = Context.OrderDescs.FirstOrDefault(o => o.PlanId == plan.Id && o.Description.Contains(daysQty));
            return JsonConvert.SerializeObject(order,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
    }
}
