﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Myaso.Models;
using Myaso.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Myaso.Services;

//using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;

namespace Myaso.Controllers
{
    [Authorize(Roles = "admin, moderator")]

    public class DishController : Controller
    {
        private ApplictionDbContext Context;
        private FileUploadService FileUploadService;
        private IHostingEnvironment Environment;

        public DishController(ApplictionDbContext applictionDbContext, FileUploadService fileUploadService, IHostingEnvironment environment)
        {
            FileUploadService = fileUploadService;
            Context = applictionDbContext;
            Environment = environment;

        }

        // GET: Dish
        public ActionResult Index()
        {
            List<Dish> dishes = Context.Dishes.ToList();
            
            return View(dishes);
        }

        public ActionResult Create()
        {
            return View();
        }

        //POST: Dish/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateDishModel model)
        {
            try
            {
                Dish isDishExist = Context.Dishes.FirstOrDefault(d => d.Name == model.Name && d.Proteins == model.Proteins && d.WeigthAndCalories == model.WeigthAndCalories);
                if (isDishExist == null)
                {
                    Context.Dishes.Add(new Dish()
                    {
                        Name = model.Name,
                        WeigthAndCalories = model.WeigthAndCalories,
                        Proteins = model.Proteins,
                        DishImagePath = $"images/{model.Name}/{model.DishImage.FileName}"
                    });
                    
                    Context.SaveChanges();
                    string path = Path.Combine(
                        Environment.WebRootPath,
                        $"images\\{model.Name}");

                    FileUploadService.Upload(path, model.DishImage.FileName, model.DishImage);
                }
                else
                {
                    return RedirectToAction("Create", "Dish"/*, "Такое блюдо уже есть"*/);
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        
        // GET: Dish/Edit/5
        public ActionResult Edit(int id)
        {
            Dish dish = Context.Dishes.First(d => d.Id == id);
            CreateDishModel model = new CreateDishModel()
            {
                Id = dish.Id,
                Name = dish.Name,
                WeigthAndCalories = dish.WeigthAndCalories,
                Proteins = dish.Proteins
            };
            return View(model);
        }

        // POST: Dish/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(CreateDishModel model) //сюда блюдо пришло, изменил его и все
        {
            try
            {
                Dish dish = Context.Dishes.First(d => d.Id == model.Id);
                dish.Name = model.Name;
                dish.WeigthAndCalories = model.WeigthAndCalories;
                dish.Proteins = model.Proteins;
                dish.DishImagePath = $"images/{model.Name}/{model.DishImage.FileName}";

                Context.SaveChanges();
                string path = Path.Combine(
                    Environment.WebRootPath,
                    $"images\\{model.Name}");

                FileUploadService.Upload(path, model.DishImage.FileName, model.DishImage);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

       

        // GET: Dish/Delete/5
        public ActionResult Delete(int id)
        {
            Dish dish = Context.Dishes.First(d => d.Id == id);
            return View(dish);
        }


        public ActionResult DeletePost(int id)
        {
            try
            {
                Dish dishContext = Context.Dishes.First(d => d.Id == id);
                Context.Entry(dishContext).State = EntityState.Deleted;
                Context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return NotFound();
            }
        }

    }
}