﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;
using Myaso.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Myaso.Controllers
{
    public class PlanController : Controller
    {
        public ApplictionDbContext Context;

        public PlanController(ApplictionDbContext applictionDbContext)
        {
            Context = applictionDbContext;
        }

        // GET: Plan
        public ActionResult Index()
        {
           // List<Plan> plans = Context.Plans.Include(p => p.OrderDesc).ToList();
            List<OrderDesc> plans = Context.OrderDescs.Include(p => p.Plan).ToList();
            return View(plans);
        }

        // GET: Plan/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Plan/Create
        public ActionResult Create()
        {
            Plan plan = new Plan();
            OrderDesc orderDesc5 = new OrderDesc();
            OrderDesc orderDesc7 = new OrderDesc();
            PlanCreateModel model = new PlanCreateModel()
            {
                Plan = plan,
                OrderDesc5 = orderDesc5,
                OrderDesc7 = orderDesc7
            };
            return View(model);
        }

        // POST: Plan/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Plan plan, OrderDesc orderDesc5, OrderDesc orderDesc7)
        {
            try
            {
                Context.Plans.Add(plan);
                Context.OrderDescs.AddRange(orderDesc5, orderDesc7);
                orderDesc5.PlanId = plan.Id;
                orderDesc7.PlanId = plan.Id;
                orderDesc5.PricePerDay = orderDesc5.PlanPrice / 5;
                orderDesc7.PricePerDay = orderDesc7.PlanPrice / 7;
                orderDesc5.PricePerDish = orderDesc5.PricePerDay / Convert.ToInt32(orderDesc5.Description);
                orderDesc7.PricePerDish = orderDesc7.PricePerDay / Convert.ToInt32(orderDesc7.Description);
                orderDesc5.Description = orderDesc5.Description + " приемов на 5 будних дней";
                orderDesc7.Description = orderDesc7.Description + " приемов на 7 дней";
                orderDesc5.TimeOfDelivery = "Бесплатная доставка: " + orderDesc5.TimeOfDelivery;
                orderDesc7.TimeOfDelivery = "Бесплатная доставка: " + orderDesc7.TimeOfDelivery;

                Context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Plan/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }
        
        // POST: Plan/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Plan/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Plan/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}