﻿using System;
using System.Linq;
using Myaso.Models;
using Myaso.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Myaso.Controllers
{
    public class OrderController : Controller
    {
        public ApplictionDbContext Context;

        public OrderController(ApplictionDbContext applictionDbContext)
        {
            Context = applictionDbContext;
        }

        // GET: Order
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult OrderDetails(string userId, string description, int planPrice, string planName)//в description будет приходить план
        {
            User user = Context.Users.FirstOrDefault(u => u.Phone == userId);
            Plan plan = Context.Plans./*Include(p => p.OrderDesc).*/First(p => p.Name == planName);
            Order order = new Order()
            {
                User = user,
                OrderDesc = Context.OrderDescs.FirstOrDefault(o => o.Description == description && o.Plan.Name == planName),
                StartDate = DateTime.Today.AddDays(1)/*.ToShortDateString()*/,
                EndDate = DateTime.Today.AddDays(7)/*.ToShortDateString()*/,
                PayConfirmed = false,
                PayConfirmedByModer = false,
                PayConfirmedByAdmin = false
            };

            OrderViewModel model = new OrderViewModel()
            {
                User = user,
                Plan = plan,
                Order = order
            };

            return View(model);
        }

        [Authorize]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult Create(string userPhone, string planName, string planDescription, DateTime planStartDate, string planEndDate, int weekQty, int id)
        {
            User user = Context.Users.FirstOrDefault(u => u.Phone == userPhone);
            Plan plan = Context.Plans.First(p => p.Name == planName);
            OrderDesc orderDesc =
                Context.OrderDescs.First(o => o.Description == planDescription && o.PlanId == plan.Id);
            if (id != 0)
            {
                Order order = Context.Orders.First(o => o.Id == id);
                DateTime newStartDate = Convert.ToDateTime(order.EndDate).AddDays(1);
                DateTime newEndDate = newStartDate.AddDays(7 * weekQty - 1);
                Order neworder = new Order()
                {
                    UserId = user.Id,
                    PlanName = planName,
                    OrderDescId = orderDesc.Id,
                    StartDate = newStartDate/*.ToShortDateString()*/,
                    EndDate = newEndDate/*.ToShortDateString()*/,
                    PayConfirmed = false,
                    PayConfirmedByModer = false,
                    PayConfirmedByAdmin = false
                };
                order.IsProlongated = true;
                Context.Orders.Add(neworder);

            }
            else
            {
                Order order = new Order()
                {
                    UserId = user.Id,
                    PlanName = planName,
                    OrderDescId = orderDesc.Id,
                    StartDate = planStartDate,
                    EndDate = Convert.ToDateTime(planStartDate).AddDays(7 * weekQty - 1)/*.ToShortDateString()*/,
                    PayConfirmed = false,
                    PayConfirmedByModer = false,
                    PayConfirmedByAdmin = false
                };
                Context.Orders.Add(order);
            }
            
            Context.SaveChanges();
            return RedirectToAction("UserProfile", "User");
        }

        // GET: Order/Edit/5
        [Authorize(Roles = "admin, moderator")]
        public IActionResult Edit(int id)
        {
            Order order = Context.Orders.First(o => o.Id == id);

            return View(order);
        }
        
        // POST: Order/Edit/5
        [HttpPost]
        [Authorize(Roles = "admin, moderator")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Order takedOrder)
        {
            try
            {
                Order order = Context.Orders.First(o => o.Id == takedOrder.Id);
                if (User.IsInRole("admin"))
                {
                    order.PayConfirmedByAdmin = takedOrder.PayConfirmedByAdmin;
                }
                else
                {
                    order.PayConfirmedByModer = takedOrder.PayConfirmedByModer;
                    //return RedirectToAction("Moderator", "User");
                }
                Context.Update(order);
                Context.SaveChangesAsync();
                return RedirectToAction("Admin", "User");
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Delete/5
        public IActionResult Delete(int id)
        {
            return View();
        }

        // POST: Order/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}