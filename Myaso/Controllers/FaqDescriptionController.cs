﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Myaso.Models;
using Myaso.ViewModels;
using Newtonsoft.Json;

namespace Myaso.Controllers
{
    public class FaqDescriptionController : Controller
    {
        private ApplictionDbContext Context;

        public FaqDescriptionController(ApplictionDbContext applictionDbContext)
        {
            Context = applictionDbContext;
        }
        // GET: FaqDescription
        public ActionResult Index(int id)
        {
            Faq faq = Context.Faqs.Include(f => f.FaqDescriptions).First(f => f.Id == id);
            FaqViewModel model = new FaqViewModel()
            {
                Faq = faq,
            };
            return View(model);
        }

        // GET: FaqDescription/Create
        public ActionResult Create(int id)
        {
            Faq faq = Context.Faqs.First(f => f.Id == id);
            CreateFaqViewModel model = new CreateFaqViewModel()
            {
                Faq = faq
            };
            return View(model);
        }

        // POST: FaqDescription/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateFaqViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Context.FaqDescriptions.Add(new FaqDescription()
                    {
                        Description = model.FaqDescription.Description,
                        Name = model.FaqDescription.Name,
                        FaqId = model.Faq.Id
                    });
                    Context.SaveChanges();
                }

                return RedirectToAction(nameof(Index), new {id = model.Faq.Id});
            }
            catch
            {
                return View();
            }
        }

        // GET: FaqDescription/Edit/5
        public ActionResult Edit(int id)
        {
            FaqDescription faq = Context.FaqDescriptions.First(f => f.Id == id);
            return View(faq);
        }

        // POST: FaqDescription/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FaqDescription faq)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Context.FaqDescriptions.Update(faq);
                    Context.SaveChanges();
                }

                return RedirectToAction(nameof(Index), new {id = faq.FaqId});
            }
            catch
            {
                return View();
            }
        }

        // GET: FaqDescription/Delete/5
        public ActionResult Delete(int id)
        {
            FaqDescription description = Context.FaqDescriptions.FirstOrDefault(f => f.Id == id);
            return View(description);
        }

        // POST: FaqDescription/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteItem(int id)
        {
            try
            {
                FaqDescription f = Context.FaqDescriptions.First(d => d.Id == id);
                Context.Entry(f).State = EntityState.Deleted;
                Context.SaveChanges();

                return RedirectToAction(nameof(Index), new { id = f.FaqId });
            }
            catch
            {
                return View();
            }
        }
    }
}