﻿using System.Linq;
using Myaso.Models;
using Microsoft.AspNetCore.Mvc;

namespace Myaso.Controllers
{
    public class ValidationController : Controller
    {
        private ApplictionDbContext Context;

        public ValidationController(ApplictionDbContext context)
        {
            Context = context;
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckLogin(string phone)
        {
            User user = Context.Users.FirstOrDefault(u => u.Phone == phone);
            return Json(user == null);
        }
    }
}   