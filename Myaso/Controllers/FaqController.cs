﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Myaso.Models;
using Newtonsoft.Json;

namespace Myaso.Controllers
{
    public class FaqController : Controller
    {
        private ApplictionDbContext Context;

        public FaqController(ApplictionDbContext applictionDbContext)
        {
            Context = applictionDbContext;
        }

        // GET: Faq
        public ActionResult Index()
        {
            IQueryable<Faq> faqs = Context.Faqs.Include(f => f.FaqDescriptions);
            return View(faqs);
        }

        // GET: Faq/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Faq/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Faq/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Faq faq)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Context.Faqs.Add(faq);
                    Context.SaveChanges();
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Faq/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Faq/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Faq faq)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Context.Faqs.Update(faq);
                    Context.SaveChanges();
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Faq/Delete/5
        public ActionResult Delete(int id)
        {
            Faq f = Context.Faqs.First(d => d.Id == id);
            return View(f);
        }

        // POST: Faq/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteItem(int id)
        {
            try
            {
                Faq f = Context.Faqs.First(d => d.Id == id);
                Context.Entry(f).State = EntityState.Deleted;
                Context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public string Test(string faqName = "Продукты")
        {
            Faq model = Context.Faqs.Include(f => f.FaqDescriptions).FirstOrDefault(f => f.Name.Contains(faqName));
            string s = JsonConvert.SerializeObject(model,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            return JsonConvert.SerializeObject(model,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
    }
}