﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Myaso.Controllers
{
    public class UserController : Controller
    {
        public ApplictionDbContext Context;

        public UserController(ApplictionDbContext applictionDbContext)
        {
            Context = applictionDbContext;
        }
        
        // GET: User
        public ActionResult UserProfile()   
        {
            User user = Context.Users.First(u => u.Phone == HttpContext.User.Identity.Name);
            List<Order> orders = Context.Orders
                .Include(o => o.User)
                .Include(o => o.OrderDesc)
                .Where(o => o.UserId == user.Id)
                .ToList();
           
            return View(orders);
        }

        // GET: User/Details/5
        [Authorize(Roles = "admin, moderator")]
        public ActionResult Admin(DateTime dateFrom, 
            DateTime dateTo, 
            bool visa,
            bool moder,
            bool adm,
            SortState sortOrder = SortState.SDateSortAsc)
        {
            //User user = Context.Users.First(u => u.Phone == HttpContext.User.Identity.Name);
            List<Order> orders = Context.Orders
                .Include(o => o.User)
                .Include(o => o.OrderDesc)
                .ToList();
            //Сортировка
            if (dateFrom != DateTime.MinValue & dateTo != DateTime.MinValue)
            {
                orders = orders.Where(o => o.StartDate >= dateFrom & o.StartDate <= dateTo).ToList();
            }
            
            if (moder)
            {
                orders = orders.Where(o => o.PayConfirmedByModer).ToList();
            }

            if (adm)
            {
                orders = orders.Where(o => o.PayConfirmedByAdmin)./*Except(orders.Where(o => o.PayConfirmedByModer)).*/ToList();
            }

            if (visa)
            {
                orders = orders.Where(o => o.PayConfirmed).ToList();
            }
            //Убрать и списка ордера которые подтвержды и админом и модером, или которые оплачены Визой
            ViewBag.StartDateSort = sortOrder == SortState.SDateSortAsc ? SortState.SDateSortDesc : SortState.SDateSortAsc;
            ViewBag.EndDateSort = sortOrder == SortState.EDateSortAsc ? SortState.EDateSortDesc : SortState.EDateSortAsc;
            ViewBag.PlanSort = sortOrder == SortState.PlanAsc ? SortState.PlanDesc : SortState.PlanAsc;
            ViewBag.UserSort = sortOrder == SortState.UserAsc ? SortState.UserDesc : SortState.UserAsc;
            ViewBag.CnfByModSort = sortOrder == SortState.CnfByModAsc ? SortState.CnfByModDesc : SortState.CnfByModAsc;
            ViewBag.CnfByAdmSort = sortOrder == SortState.CnfByAdmAsc ? SortState.CnfByAdmDesc : SortState.CnfByAdmAsc;
            ViewBag.VizaSort = sortOrder == SortState.VizaAsc ? SortState.VizaDesc : SortState.VizaAsc;

            switch (sortOrder)
            {
                case SortState.SDateSortDesc:
                    orders = orders.OrderByDescending(s => s.StartDate).ToList();
                    break;
                case SortState.EDateSortAsc:
                    orders = orders.OrderBy(s => s.EndDate).ToList();
                    break;
                case SortState.EDateSortDesc:
                    orders = orders.OrderByDescending(s => s.EndDate).ToList();
                    break;
                case SortState.PlanAsc:
                    orders = orders.OrderBy(s => s.PlanName).ToList();
                    break;
                case SortState.PlanDesc:
                    orders = orders.OrderByDescending(s => s.PlanName).ToList();
                    break;
                case SortState.UserAsc:
                    orders = orders.OrderBy(s => s.User.Phone).ToList();
                    break;
                case SortState.UserDesc:
                    orders = orders.OrderByDescending(s => s.User.Phone).ToList();
                    break;
                case SortState.CnfByModAsc:
                    orders = orders.OrderBy(s => s.PayConfirmedByModer).ToList();
                    break;
                case SortState.CnfByModDesc:
                    orders = orders.OrderByDescending(s => s.PayConfirmedByModer).ToList();
                    break;
                case SortState.CnfByAdmAsc:
                    orders = orders.OrderBy(s => s.PayConfirmedByAdmin).ToList();
                    break;
                case SortState.CnfByAdmDesc:
                    orders = orders.OrderByDescending(s => s.PayConfirmedByAdmin).ToList();
                    break;
                case SortState.VizaAsc:
                    orders = orders.OrderBy(s => s.PayConfirmed).ToList();
                    break;
                case SortState.VizaDesc:
                    orders = orders.OrderByDescending(s => s.PayConfirmed).ToList();
                    break;
                default:
                    orders = orders.OrderBy(s => s.StartDate).ToList();
                    break;
            }
            return View(orders);
        }
        
        // GET: User/Details/5
        [Authorize(Roles = "moderator")]
        public ActionResult Moderator()
        {
            //User user = Context.Users.First(u => u.Phone == HttpContext.User.Identity.Name);
            List<Order> orders = Context.Orders
                .Include(o => o.User)
                .Include(o => o.OrderDesc)
                .ToList();

            return View(orders);
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(UserProfile));
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        //public ActionResult OrderProlongate(int id)
        //{
        //    Order order = Context.Orders
        //        .Include(o => o.User)
        //        .First(o => o.Id == id);//здесь можно сделать начало поставить время окончания предыдущего ордера
        //    DateTime newStartDate = Convert.ToDateTime(order.EndDate).AddDays(1);
        //    order.StartDate = newStartDate.ToShortDateString();
        //    return View(order);
        //}

        //// POST: User/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult OrderProlongate(int weekQty, int id/*IFormCollection collection*/)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(UserProfile));
            }
            catch
            {
                return View();
            }
        }
    }
}