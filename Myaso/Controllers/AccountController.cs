﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Myaso.Models;
using Myaso.Services;
using Myaso.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Myaso.Controllers
{
    public class AccountController : Controller
    {
        private ApplictionDbContext db;
        private IHostingEnvironment environment;
        private FileUploadService fileUploadService;

        public AccountController(
            ApplictionDbContext context,
            IHostingEnvironment environment,
            FileUploadService fileUploadService
            )
        {
            db = context;
            this.environment = environment;
            this.fileUploadService = fileUploadService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(string phone, string password)
        {
            if (ModelState.IsValid)//Удалить модель
            {
                User user = await db.Users.Include(u => u.Role).FirstOrDefaultAsync(u => u.Phone == phone && u.Password == password);
                if (user != null)
                {
                    await Authenticate(user); // аутентификация

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await db.Users.Include(u => u.Role).FirstOrDefaultAsync(u => u.Phone == model.Phone);

                if (user == null)
                {
                    db.Users.Add(new User
                    {
                        Phone = model.Phone,
                        //Email = model.Email,
                        Password = model.Password,
                        RoleId = 1
                        //AvatarImage = $"images/{model.Login}/avatar/{model.AvatarImage.FileName}"

                    });
                    await db.SaveChangesAsync();

                    // string path = Path.Combine(
                    //environment.WebRootPath,
                    //$"images\\{model.Login}\\avatar");

                    //fileUploadService.Upload(path, model.AvatarImage.FileName, model.AvatarImage);

                    await Authenticate(db.Users.Include(u => u.Role).FirstOrDefault(u => u.Phone == model.Phone));

                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Phone),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name)
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }
    }
}