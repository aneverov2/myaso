﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;
using Myaso.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Myaso.Controllers
{
    [Authorize(Roles = "admin, moderator")]

    public class MenuController : Controller
    {
        public ApplictionDbContext Context;

        public MenuController(ApplictionDbContext applictionDbContext)
        {
            Context = applictionDbContext;
        }

        // GET: Week
        public ActionResult Index()
        {
            List<PlanToDay> planToDays = Context.PlanToDays
                .Include(p => p.Day)
                .Include(p => p.Week)
                .Include(p => p.Plan)
                .ToList();
            List<DishToPlan> dishToPlans = Context.DishToPlans
                .Include(d => d.Dish)
                .Include(d => d.Food)
                .Include(d => d.PlanToDay)
                .ThenInclude(p => p.Plan)
                .ToList();
            MenuViewModel model = new MenuViewModel()
            {
                //PlanToDays = planToDays,
                DishToPlans = dishToPlans
            };
            return View(model);
        }

        // GET: Week/Details/5
        public ActionResult Details(int id)
        {

            return View();
        }

        // GET: Week/Create
        public ActionResult Create()
        {
            MenuViewModel model = new MenuViewModel();
            ViewBag.foods = new SelectList(Context.Foods.ToList(), "Id", "Name");
            ViewBag.Dishes = new SelectList(Context.Dishes.ToList(), "Id", "Name");
            ViewBag.Plans = new SelectList(Context.Plans.ToList(), "Id", "Name");
            ViewBag.Days = new SelectList(Context.Days.ToList(), "Id", "Name");
            ViewBag.Weeks = new SelectList(Context.Weeks.ToList(), "Id", "Name");
            return View(model);
        }

        // POST: Week/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MenuViewModel model)
        {
            try
            {
                // TODO: Разобраться - как лучше сделать составление меню!!! Поудобнее

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Week/Edit/5
        public ActionResult Edit(int id)
        {
            List<PlanToDay> planToDays = Context.PlanToDays
                .Include(p => p.Day)
                .Include(p => p.Week)
                .Include(p => p.Plan)
                .ToList();
            List<DishToPlan> dishToPlans = Context.DishToPlans
                .Include(d => d.Dish)
                .Include(d => d.Food)
                .Include(d => d.PlanToDay)
                .ThenInclude(p => p.Plan)
                .ToList();
            MenuViewModel model = new MenuViewModel()
            {
                DishToPlans = dishToPlans,
            };
            ViewBag.foods = new SelectList(Context.Foods.ToList(), "Id", "Name");
            ViewBag.Dishes = new SelectList(Context.Dishes.ToList(), "Id", "Name");
            ViewBag.Plans = new SelectList(Context.Plans.ToList(), "Id", "Name");
            ViewBag.Days = new SelectList(Context.Days.ToList(), "Id", "Name");
            ViewBag.Weeks = new SelectList(Context.Weeks.ToList(), "Id", "Name");
            return View(model);
        }

        // POST: Week/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int newdishId, int newfoodId, int newdayId, int newweekId , int newplanId)
        {
            try
            {
                PlanToDay plan = Context.PlanToDays.First(p =>
                    p.PlanId == newplanId && p.DayId == newdayId && p.WeekId == newweekId);
                DishToPlan dish = Context.DishToPlans.FirstOrDefault(d =>
                    d.DishId == newdishId && d.FoodId == newfoodId && d.PlanToDayId == plan.Id);
                if (dish == null)
                {
                    DishToPlan dishToPlan = new DishToPlan()
                    {
                        DishId = newdishId,
                        FoodId = newfoodId,
                        PlanToDayId = plan.Id
                    };
                    Context.DishToPlans.Add(dishToPlan);
                    Context.SaveChanges();
                    return RedirectToAction(nameof(Edit));
                }
                else
                {
                    return StatusCode(418);
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Week/Delete/5
        public ActionResult Delete(int id)
        {
            List<PlanToDay> planToDays = Context.PlanToDays
                .Include(p => p.Day)
                .Include(p => p.Week)
                .Include(p => p.Plan)
                .ToList();
            List<DishToPlan> dishToPlans = Context.DishToPlans
                .Include(d => d.Dish)
                .Include(d => d.Food)
                .Include(d => d.PlanToDay)
                .ThenInclude(p => p.Plan)
                .ToList();
            MenuViewModel model = new MenuViewModel()
            {
                //PlanToDays = planToDays,
                DishToPlans = dishToPlans,
            };
            ViewBag.foods = new SelectList(Context.Foods.ToList(), "Id", "Name");
            ViewBag.Dishes = new SelectList(Context.Dishes.ToList(), "Id", "Name");
            ViewBag.Plans = new SelectList(Context.Plans.ToList(), "Id", "Name");
            ViewBag.Days = new SelectList(Context.Days.ToList(), "Id", "Name");
            ViewBag.Weeks = new SelectList(Context.Weeks.ToList(), "Id", "Name");
            return View(model);
        }

        // POST: Week/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int newdishId, int newfoodId, int newdayId, int newweekId, int newplanId)
        {
            try
            {
                PlanToDay plan = Context.PlanToDays.First(p =>
                    p.PlanId == newplanId && p.DayId == newdayId && p.WeekId == newweekId);

                DishToPlan dishToPlan = new DishToPlan()
                {
                    DishId = newdishId,
                    FoodId = newfoodId,
                    PlanToDayId = plan.Id
                };
                Context.DishToPlans.Remove(dishToPlan);
                Context.SaveChanges();

                return RedirectToAction(nameof(Edit));
            }
            catch
            {
                return View();
            }
        }
    }
}