﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Myaso.Migrations
{
    public partial class AddProteinsField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Proteins",
                table: "Dishes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WeigthAndCalories",
                table: "Dishes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Proteins",
                table: "Dishes");

            migrationBuilder.DropColumn(
                name: "WeigthAndCalories",
                table: "Dishes");
        }
    }
}
