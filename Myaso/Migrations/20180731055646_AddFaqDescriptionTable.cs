﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Myaso.Migrations
{
    public partial class AddFaqDescriptionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FaqDescription",
                table: "Faqs");

            migrationBuilder.DropColumn(
                name: "FaqName",
                table: "Faqs");

            migrationBuilder.RenameColumn(
                name: "FaqQuestion",
                table: "Faqs",
                newName: "Name");

            migrationBuilder.CreateTable(
                name: "FaqDescriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    FaqId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaqDescriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FaqDescriptions_Faqs_FaqId",
                        column: x => x.FaqId,
                        principalTable: "Faqs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FaqDescriptions_FaqId",
                table: "FaqDescriptions",
                column: "FaqId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FaqDescriptions");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Faqs",
                newName: "FaqQuestion");

            migrationBuilder.AddColumn<string>(
                name: "FaqDescription",
                table: "Faqs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FaqName",
                table: "Faqs",
                nullable: true);
        }
    }
}
