﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Myaso.Migrations
{
    public partial class test1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DishToPlans_PlanToDays_PlanToDayId",
                table: "DishToPlans");

            migrationBuilder.DropColumn(
                name: "DishToPlanId",
                table: "DishToPlans");

            migrationBuilder.AlterColumn<int>(
                name: "PlanToDayId",
                table: "DishToPlans",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DishToPlans_PlanToDays_PlanToDayId",
                table: "DishToPlans",
                column: "PlanToDayId",
                principalTable: "PlanToDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DishToPlans_PlanToDays_PlanToDayId",
                table: "DishToPlans");

            migrationBuilder.AlterColumn<int>(
                name: "PlanToDayId",
                table: "DishToPlans",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "DishToPlanId",
                table: "DishToPlans",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_DishToPlans_PlanToDays_PlanToDayId",
                table: "DishToPlans",
                column: "PlanToDayId",
                principalTable: "PlanToDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
