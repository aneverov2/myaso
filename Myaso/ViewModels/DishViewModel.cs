﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Data.OData.Query.SemanticAst;

namespace Myaso.ViewModels
{
    public class DishViewModel
    {
        public Dish Dish { get; set; }
        public SelectList Foods { get; set; }
        public SelectList Plans { get; set; }
    }
}
