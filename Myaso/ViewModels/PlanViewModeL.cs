﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;

namespace Myaso.ViewModels
{
    public class PlanViewModel
    {
        public User User { get; set; }
        public Plan Plan { get; set; }
        public OrderDesc OrderDesc { get; set; }
        public Day Day { get; set; }
        public Faq Faq { get; set; }
        public Week Week { get; set; }
        public PlanToDay PlanToDay { get; set; }

        public List<Day> Days { get; set; }
        public List<Faq> Faqs { get; set; }
        public List<FaqDescription> FaqDescriptions { get; set; }
        public List<Plan> Plans { get; set; }
        public List<DishToPlan> DishToPlans { get; set; }
        public List<Week> Weeks { get; set; }
        public DateTime FirstDay { get; set; }

        public PlanViewModel()
        {
            Days = new List<Day>();
            Faqs = new List<Faq>();
            DishToPlans = new List<DishToPlan>();
        }
    }
}
