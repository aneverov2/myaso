﻿using Myaso.Models;

namespace Myaso.ViewModels
{
    public class CreateFaqViewModel
    {
        public FaqDescription FaqDescription { get; set; }
        public Faq Faq { get; set; }
    }
}
