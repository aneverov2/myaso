﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;

namespace Myaso.ViewModels
{
    public class PlanCreateModel
    {
        public Plan Plan { get; set; }
        public OrderDesc OrderDesc5 { get; set; }
        public OrderDesc OrderDesc7 { get; set; }
    }
}
