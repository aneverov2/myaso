﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;

namespace Myaso.ViewModels
{
    public class OrderViewModel
    {
        public User User { get; set; }
        public Plan Plan { get; set; }
        public Order Order { get; set; }    
    }
}
