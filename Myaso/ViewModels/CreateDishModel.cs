﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Myaso.ViewModels
{
    public class CreateDishModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string WeigthAndCalories { get; set; }
        [Required]
        public string Proteins { get; set; }
        [Required]
        public IFormFile DishImage { get; set; }
    }
}
