﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Myaso.ViewModels
{
    public class MenuViewModel
    {
        public int NewplanId { get; set; }
        public int NewdishId { get; set; }
        public int NewfoodId { get; set; }
        public int NewdayId { get; set; }
        public int NewweekId { get; set; }
        //public List<PlanToDay> PlanToDays { get; set; }
        public List<DishToPlan> DishToPlans { get; set; }
        //public SelectList Foods { get; set; }
        //public SelectList Dishes { get; set; }
        //public SelectList Plans { get; set; }
        //public SelectList Days { get; set; }
        //public SelectList Weeks { get; set; }
        public MenuViewModel()
        {
            //PlanToDays = new List<PlanToDay>();
            DishToPlans = new List<DishToPlan>();
        }
    }
}
