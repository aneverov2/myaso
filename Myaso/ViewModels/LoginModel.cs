﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Myaso.ViewModels
{
    public class LoginModel
    {
        [Required]
        public string Phone { get; set; }   

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
