﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Myaso.ViewModels
{
    public class ListDishViewModel
    {
        public List<Dish> Dishes { get; set; }
        public Food Food { get; set; }
        public Plan Plan { get; set; }
        //public SelectList Foods { get; set; }
        //public SelectList Plans { get; set; }
    }
}
