﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;

namespace Myaso.ViewModels
{
    public class FaqViewModel
    {
        public Faq Faq { get; set; }
        public IQueryable<FaqDescription> FaqDescriptions { get; set; } 
    }
}
