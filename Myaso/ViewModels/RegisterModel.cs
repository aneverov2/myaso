﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Schema;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Myaso.ViewModels
{
    public class RegisterModel
    {
        [Required]
        [Phone]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Поле Phone должно быть цифровым и состоять из 10 цифр, например 0555235400")]
        [Remote("CheckLogin", "Validation", ErrorMessage = "Логин уже занят")]
        public string Phone { get; set; }

        //[Required]
        //[DataType(DataType.EmailAddress)]
        //public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string PasswordConfirm { get; set; }
    }
}
