﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Myaso.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public int? RoleId { get; set; }    
        public Role Role { get; set; }

        public List<Order> UserToOrders { get; set; }

        public User()
        {
            UserToOrders = new List<Order>();
        }
    }
}
