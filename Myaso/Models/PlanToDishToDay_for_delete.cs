﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class PlanToDishToDay
    {
        [Key]
        public int Id { get; set; }

        public int PlanId { get; set; }
        public Plan Plan { get; set; }

        public int DishId { get; set; }
        public Dish Dish { get; set; }

        public int DayId { get; set; }
        public Day Day { get; set; }

        public int WeekId { get; set; }
        //public Week Week { get; set; }
    }
}
