﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class Food
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public List<DishToPlan> DishToPlans { get; set; }

        public Food()
        {
            DishToPlans = new List<DishToPlan>();
        }
    }
}
