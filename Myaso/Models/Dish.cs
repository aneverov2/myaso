﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class Dish
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string WeigthAndCalories { get; set; }
        public string Proteins { get; set; }
        public string DishImagePath { get; set; }

        public List<DishToPlan> DishToPlans { get; set; }

        public Dish()
        {
            DishToPlans = new List<DishToPlan>();
        }
    }
}
