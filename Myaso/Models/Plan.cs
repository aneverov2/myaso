﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class Plan
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int CaloriesQuantity { get; set; }
        public string Svg { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        //public int OrderDescId { get; set; }
        //public OrderDesc OrderDesc { get; set; }

        public List<PlanToDay> PlanToDays { get; set; }

        public Plan()   
        {
            PlanToDays = new List<PlanToDay>();
        }
    }
}
