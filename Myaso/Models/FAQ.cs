﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class Faq
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<FaqDescription> FaqDescriptions { get; set; }

    }
}
