﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class Day    
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameEn { get; set; }


        public List<PlanToDay> PlanToDays { get; set; }

        public Day()
        {
            PlanToDays = new List<PlanToDay>();
        }
    }
}
