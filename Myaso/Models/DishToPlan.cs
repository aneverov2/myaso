﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class DishToPlan
    {
        public int Id { get; set; }
        
        public int DishId { get; set; }
        public Dish Dish { get; set; }

        public int FoodId { get; set; }
        public Food Food { get; set; }

        public int PlanToDayId { get; set; }
        public PlanToDay PlanToDay { get; set; }

        
    }
}
