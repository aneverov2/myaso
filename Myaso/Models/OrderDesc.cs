﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class OrderDesc
    {
        public int Id { get; set; }

        public int PlanId { get; set; }
        public Plan Plan { get; set; }
        public string Description { get; set; }
        public int PlanPrice { get; set; }
        public int PricePerDay { get; set; }
        public int PricePerDish { get; set; }
        public string TimeOfDelivery { get; set; }


    }
}
