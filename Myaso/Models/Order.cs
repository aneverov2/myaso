﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public enum SortState
    {
        SDateSortAsc,
        SDateSortDesc,
        EDateSortAsc,
        EDateSortDesc,
        PlanAsc,
        PlanDesc,
        UserAsc,
        UserDesc,
        CnfByModAsc,
        CnfByModDesc,
        CnfByAdmAsc,
        CnfByAdmDesc,
        VizaAsc,
        VizaDesc
    }
    public class Order
    {
        [Key]
        public int Id { get; set; }
        //public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PlanName { get; set; }
        public bool PayConfirmed { get; set; }
        public bool PayConfirmedByModer { get; set; }
        public bool PayConfirmedByAdmin { get; set; }
        public bool IsProlongated { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int OrderDescId { get; set; }
        public OrderDesc OrderDesc { get; set; }

        
    }
}
