﻿using Microsoft.EntityFrameworkCore;

namespace Myaso.Models
{
    public class ApplictionDbContext : DbContext
    {
        public DbSet<Day> Days { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Faq> Faqs { get; set; }
        public DbSet<FaqDescription> FaqDescriptions { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<Week> Weeks { get; set; }
        public DbSet<OrderDesc> OrderDescs { get; set; }
        public DbSet<DishToPlan> DishToPlans { get; set; }
        public DbSet<PlanToDay> PlanToDays { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        //public DbSet<Order> UserToOrders { get; set; }


        public ApplictionDbContext(DbContextOptions<ApplictionDbContext> options)
            : base(options)
        {
        }
    }
}
