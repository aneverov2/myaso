﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class PlanToDay
    {
        [Key]
        public int Id { get; set; }

        public int PlanId { get; set; }
        public Plan Plan { get; set; }

        public int DayId { get; set; }
        public Day Day { get; set; }

        public int WeekId { get; set; }
        public Week Week { get; set; }

        public List<DishToPlan> DishToPlans { get; set; }

        public PlanToDay()
        {
            DishToPlans = new List<DishToPlan>();
        }
    }
}
