﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Myaso.Models
{
    public class FaqDescription
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int FaqId { get; set; }
        public Faq Faq { get; set; }    
    }
}
