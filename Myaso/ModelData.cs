﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Myaso.Models;

namespace Myaso
{
    public class ModelData
    {
        public static void Initialize(ApplictionDbContext context)
        {
            if (!context.Plans.Any())
            {
                context.Plans.AddRange
                (
                    new Plan() { Name = "Fit", CaloriesQuantity = 1200 },
                    new Plan() { Name = "Power", CaloriesQuantity = 2500 },
                    new Plan() { Name = "Daily", CaloriesQuantity = 1400 },
                    new Plan() { Name = "Balance", CaloriesQuantity = 2000 },
                    new Plan() { Name = "Vega", CaloriesQuantity = 1500 }
                );
                context.SaveChanges();
            }
            if (!context.Days.Any())
            {
                context.Days.AddRange
                (
                    new Day() { Name = "Пн" },
                    new Day() { Name = "Вт" },
                    new Day() { Name = "Ср" },
                    new Day() { Name = "Чт" },
                    new Day() { Name = "Пт" },
                    new Day() { Name = "Сб" },
                    new Day() { Name = "Вс" }
                );
                context.SaveChanges();
            }
            
            if (!context.Foods.Any())
            {
                for (int i = 0; i < 6; i++)
                {
                    context.Foods.Add(new Food()
                    {
                        Name = "Прием №" + (i + 1)
                    });
                }
                context.SaveChanges();
            }

            if (!context.Dishes.Any())
            {
                int count = 1;
                for (int i = 0; i < 56; i++)
                {
                    context.Dishes.Add(new Dish()
                    {
                        Name = "Блюдо №" + count
                    });
                    count++;
                }
                context.SaveChanges();
            }

            if (!context.Weeks.Any())
            {
                int count = 1;
                for (int i = 0; i < 3; i++)
                {
                    context.Weeks.Add(new Week()
                    {
                        Name = "Неделя №" + count
                    });
                    count++;
                }
                context.SaveChanges();
            }

            if (!context.PlanToDays.Any())
            {
                int week = 1;
                List<Plan> plans = context.Plans.ToList();
                List<Day> days = context.Days.ToList();

                while (week < 3)
                {
                    foreach (Plan plan in plans)
                    {
                        for (int i = 1; i <= days.Count; i++)
                        {
                            context.PlanToDays.Add(new PlanToDay()
                            {
                                PlanId = plan.Id,
                                DayId = i,
                                WeekId = week
                            });
                        }
                        context.SaveChanges();
                    }

                    week++;
                }
                context.SaveChanges();
            }
        }
    }
}
